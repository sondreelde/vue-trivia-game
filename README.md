# vue-trivia-game

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Components

### GameMenu
```

Entering the website shows this component.
The player enters a name.
He can then press a button to start the game.
```

### GamePlay
```

This component handles the game such as questions and answers.
The QuizQuestion component is diplayed here.
```

### QuizQuestion
```

This component simply displays a question and lets the player choose an alternative answer.
The answer is then emitted to the GamePlay component.
```

### GameOver
```

After all the quesitons is answerd the player is shown this component.
It displays the QuizResults component and gives the player an option to
go back to the GameMenu or try again with new questions.
```

### QuizResults
```

Displays the score, all the questions, correct answers and the players answer.
```