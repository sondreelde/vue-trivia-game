import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import GameMenu from './components/GameMenu.vue'
import GamePlay from './components/GamePlay.vue'
import GameOver from './components/GameOver.vue'

Vue.use(BootstrapVue)
Vue.use(VueRouter)

Vue.config.productionTip = false

const routes = [
  { path: '/', name: 'menu', component: GameMenu },
  { path: '/play', name: 'play', component: GamePlay },
  { path: '/over', name: 'over', component: GameOver }
]

const router = new VueRouter({
  routes: routes,
  mode: 'history'
})

new Vue({
  render: h => h(App),
  router: router
}).$mount('#app')
