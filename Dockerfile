FROM alpine

WORKDIR /src
COPY package.json .
RUN npm i
COPY . .

RUN npm run build

FROM alpine AS production

ENV PORT 3000

WORKDIR /app
RUN npm install express morgan
COPY index.js . 
COPY --from=builder /src/dist ./dist

EXPOSE 3000/tcp

CMD [ "node", "index.js" ]